import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { Container, Grid, Input, Button, MenuList, MenuItem } from '@mui/material';

const Task = () => {
    const dispatch = useDispatch();

    const { tasks, taskName }  = useSelector((reduxData) => reduxData.taskReducer);

    const taskNameHandler = event => {
      dispatch({
        type: "VALUE_HANDLER",
        payload: {
          taskName: event.target.value
        }
      });
    };
  
    const addTaskHandler = event => {
      event.preventDefault();
      let id = 0;

      // Hàm xử lý để lấy id cho task mới
      if (tasks.length) {
        id = Math.max(...tasks.map(task => task.id));
    
        id++;
      }
  
      if (taskName) {
        dispatch({
          type: "ADD_TASK",
          payload: {
            task: {
              name: taskName,
              completed: false,
              id
            }
          }
        });
        dispatch({
          type: "VALUE_HANDLER",
          payload: {
            taskName: ""
          }
        });
      }
    };
  
    const taskCompletedHandler = event => {
      dispatch({
        type: "TOGGLE_COMPLETED_TASK",
        payload: {
          id: event.target.id
        }
      });
    };
  
    return (
      <Container>
        <Grid container spacing={2} style={{margin: '0 auto', padding: 20}}>
          <Grid item xs={12}>
            <form onSubmit={addTaskHandler}>
              <Grid container xs={12}>
                <Grid item xs={8}>
                  <Input value={taskName} onChange={taskNameHandler} style={{width: '90%'}}/>
                </Grid>
                <Grid item xs={4}>
                  <Button variant="contained" type="submit">Add Task</Button>
                </Grid>
              </Grid>
            </form>
          </Grid>
        </Grid>
      
        <MenuList>
          {tasks.map((task, index) => {
            return (
              <MenuItem
                key={task.id}
                style={{
                  color: task.completed ? "green" : "red"
                }}
                onClick={taskCompletedHandler}
                id={task.id}
              > 
                {index + 1}. {task.name}
              </MenuItem>
            );
          })}
        </MenuList>
      </Container>
    );
}
  
export default Task;

